/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.pavelk.admint;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import static java.lang.System.in;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.ini4j.Ini;

/**
 *
 * @author pavelk
 */
public class ADMintController implements Initializable  {
    
    @FXML
    private TextField pcNameEdit = new TextField();
    
    @FXML
    private TextField workGroupEdit = new TextField();
    
    @FXML
    private TextField domainEdit = new TextField();

    @FXML
    private TextField dcIPEdit = new TextField();
    
    @FXML
    private TextField dcNameEdit = new TextField();
    
    @FXML
    private TextField dc2IPEdit = new TextField();
    
    @FXML
    private TextField dc2NameEdit = new TextField();
    
    @FXML
    private TextField NTPNameEdit = new TextField();

    private ArrayList<String> stringList = new ArrayList();
    
    String hostName;
    String workGroup;
    String fullDomain; 
    String bigFullDomain;
    String dcName;
    String dc2Name;
    String fullDcName;
    String fullDc2Name;
    String dcIp;
    String dc2Ip;
    String nameserverDcIp;
    String nameserverDc2Ip;
    String kdc;
    String kdc2;
    String timeServer;
    
    //private static final Pattern IP = Pattern.compile("^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loadDefaultSettings();
    /*    dcIPEdit.textProperty().addListener((observable, oldValue, newValue) -> {
           if(!newValue.matches("^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$")){
               dc2IPEdit.setText(oldValue);
           }
        });*/
    }
    
    private String pathToJar(){
        try {
            return new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile().getAbsolutePath();
        } catch (URISyntaxException ex) {
            Logger.getLogger(ADMintController.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }
    
    private void loadDefaultSettings() {
         pcNameEdit.setText(loadHostName());
        try {
            Ini ini = new Ini(new File(pathToJar() + "/ADMint.defaults"));
            //pcNameEdit.setText(ini.get("settings", "pcname"));
           
            workGroupEdit.setText(ini.get("settings", "workgroup"));
            domainEdit.setText(ini.get("settings", "domain"));
            dcIPEdit.setText(ini.get("settings", "dcip"));
            dcNameEdit.setText(ini.get("settings", "dcname"));
            dc2IPEdit.setText(ini.get("settings", "dc2ip"));
            dc2NameEdit.setText(ini.get("settings", "dc2name"));
            NTPNameEdit.setText(ini.get("settings", "timeserver"));
          
        } catch (IOException ex) {
            Logger.getLogger(ADMintController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void loadTemplates(){
        hostName = pcNameEdit.getText().trim();
        workGroup = workGroupEdit.getText().toUpperCase();
        fullDomain = domainEdit.getText().toLowerCase();
        bigFullDomain = domainEdit.getText().toUpperCase();
        dcIp = dcIPEdit.getText().trim();
        dcName =dcNameEdit.getText().toLowerCase().trim();
        dc2Ip = dc2IPEdit.getText().trim();
        dc2Name =dc2NameEdit.getText().toLowerCase().trim();
        nameserverDcIp = "nameserver " + dcIp;
        nameserverDc2Ip=((dc2Ip==null)||(dc2Ip.equals("")))?"":"nameserver " + dc2Ip;
        kdc = "kdc = " + dcName + "." + fullDomain;
        kdc2 = ((dc2Name==null)||(dc2Ip.equals("")))?"":"kdc = " + dc2Name + "." + fullDomain;
        fullDcName = dcName + "." + fullDomain;
        fullDc2Name = ((dc2Name==null)||(dc2Ip.equals("")))?"":dc2Name + "." + fullDomain;
        timeServer = (NTPNameEdit.getText().trim().equals(""))?"":"server "+NTPNameEdit.getText().trim().toLowerCase();
        
        
        System.out.println("hostname="+hostName);
        System.out.println("workgroup="+workGroup);
        System.out.println("fullDomain="+fullDomain);
        System.out.println("bigFullDomain="+bigFullDomain);
        System.out.println("DC ip="+dcIp);
        System.out.println("DC name="+dcName);
        System.out.println("DC 2 ip="+dc2Ip);
        System.out.println("DC 2 name="+dc2Name);
        System.out.println("Nameserver="+nameserverDcIp);
        System.out.println("Nameserver2="+nameserverDc2Ip);
        System.out.println("KDC="+kdc);
        System.out.println("KDC2="+kdc2);
        System.out.println("ntp="+timeServer);
        
    }
    
    
    private void loadFileFromJar(String fileName) throws IOException{
        InputStream in = this.getClass().getResourceAsStream(fileName);
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line;
        stringList.clear();
        while ((line = reader.readLine()) != null) {
            stringList.add(line);
        }
    }
    
     
    private String loadHostName(){
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader("/etc/hostname"));
        
        String line;
          return reader.readLine();
        } catch (IOException ex) {
            Logger.getLogger(ADMintController.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
            
    
    }
    
    private void changeTemplate(String template, String variable){
        Iterator<String> i = stringList.iterator();
        ArrayList<String> al = new ArrayList();
        while (i.hasNext())
           al.add(i.next().replace(template, variable));
        stringList = al;
    }
    
    private void writeFile(String fileName) throws FileNotFoundException, IOException{
        
        File jarDir = new File(pathToJar()+"/ADMint/");
        jarDir.mkdirs();
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
              new FileOutputStream(pathToJar()+"/ADMint/"+fileName)))) {
         Iterator<String> i = stringList.iterator();
         while (i.hasNext())
              writer.write(i.next()+"\n");
}
    }
    
    private void uploadConfigs() throws IOException{
        loadFileFromJar("templates/head.template");
        changeTemplate("<full-domain>",fullDomain);
        changeTemplate("<nameserver-dc-ip>",nameserverDcIp);
        changeTemplate("<nameserver-dc2-ip>",nameserverDc2Ip);
        writeFile("head");
        
        loadFileFromJar("templates/hostname.template");
        changeTemplate("<host-name>",hostName);
        writeFile("hostname");
        
        loadFileFromJar("templates/hosts.template");
        changeTemplate("<host-name>",hostName);
        changeTemplate("<full-domain>", fullDomain);
        changeTemplate("<dc-ip>", dcIp);
        changeTemplate("<dc2-ip>", dc2Ip);
        changeTemplate("<dc-name>", dcName);
        changeTemplate("<dc2-name>", dc2Name);
        changeTemplate("<full-dc-name>", fullDcName);
        changeTemplate("<full-dc2-name>", fullDc2Name);
        writeFile("hosts");
        
        loadFileFromJar("templates/join.sh.template");
        writeFile("join.sh");
        
        loadFileFromJar("templates/krb5.conf.template");
        changeTemplate("<big-full-domain>", bigFullDomain);
        changeTemplate("<full-domain>", fullDomain);
        changeTemplate("<dc-name>", dcName);
        changeTemplate("<kdc>", kdc);
        changeTemplate("<kdc2>", kdc2);
        writeFile("krb5.conf");
        
        loadFileFromJar("templates/limits.conf.template");
        writeFile("limits.conf");
        
        loadFileFromJar("templates/nsswitch.conf.template");
        writeFile("nsswitch.conf");
        
        loadFileFromJar("templates/ntp.conf.template");
        changeTemplate("<time-server>", timeServer);
        writeFile("ntp.conf");
        
        loadFileFromJar("templates/smb.conf.template");
        changeTemplate("<work-group>", workGroup);
        changeTemplate("<big-full-domain>", bigFullDomain);
        writeFile("smb.conf");
        
        loadFileFromJar("templates/common-session.template");
        writeFile("common-session");
        
    }
        

    @FXML
    private void onGoClick(ActionEvent event) throws IOException {
        
        loadTemplates();
        uploadConfigs();
       // Runtime.getRuntime().exec("usr/bin/xterm");
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Выполнено!");
        alert.setHeaderText("Компьютер готов к введнию в домен");
        alert.setContentText("Запустите скрипт ./ADMint/join.sh с правами su \nи следуйте инструкциям\n sudo sh ./join.sh");
        alert.showAndWait();

        
        
        
        
    }

}
